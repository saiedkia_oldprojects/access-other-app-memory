﻿using System;
using System.IO.MemoryMappedFiles;
using System.Threading;

namespace Client
{
    public class Program
    {
        static int num;
        static MemoryMappedFile mappedFile;
        static MemoryMappedViewAccessor memory;
        public static void Main(string[] args)
        {
            // wait for map file creation by host app
            Thread.Sleep(1000);
            mappedFile = MemoryMappedFile.OpenExisting("mapFile.mp");
            memory = mappedFile.CreateViewAccessor();

            ConsoleKeyInfo KeyInfo = new ConsoleKeyInfo();
            while (true)
            {
                Console.WriteLine("press any key to see changes or ESC to exit.");
                KeyInfo = Console.ReadKey();
                if (KeyInfo.Key == ConsoleKey.Escape)
                    break;

                byte[] buffer = new byte[1024];
                int count = memory.ReadArray<byte>(0, buffer, 0, buffer.Length);
                Console.WriteLine("Value: " + System.Text.Encoding.ASCII.GetString(buffer).Trim());
            }
        }
    }
}
