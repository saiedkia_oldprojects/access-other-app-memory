﻿using System;
using System.Diagnostics;
using System.IO.MemoryMappedFiles;
using System.Windows.Forms;
//https://docs.microsoft.com/en-us/dotnet/standard/io/memory-mapped-files
namespace AccessOtherApp
{
    public partial class Form1 : Form
    {
        //Process process;
        MemoryMappedFile mappedFile;
        MemoryMappedViewAccessor memory;
        public Form1()
        {
            InitializeComponent();

            mappedFile = MemoryMappedFile.CreateNew("mapFile.mp", 2048);
            memory = mappedFile.CreateViewAccessor();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //process = Process.Start(Application.StartupPath + @"\client.exe");
        }

        private void btnSetValue_Click(object sender, EventArgs e)
        {
            //if (process == null)
            //{

            //    MessageBox.Show("please first start client app");
            //    return;
            //}


            byte[] buffer = new byte[1024];
            byte[] data = System.Text.Encoding.ASCII.GetBytes(textBox1.Text + "\0");

            Array.Copy(data, buffer, data.Length);
            memory.WriteArray<byte>(0, buffer, 0, buffer.Length);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }
    }
}
